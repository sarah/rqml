use orbtk::prelude::*;

use rqml::parser::parse_qml;
use rqml::MainViewState;

fn main() {
    Application::new()
        .window(|ctx| {
            let qml_doc = parse_qml("./res/example.qml");
            let top_level = qml_doc.children.clone();
            println!("{:?}", qml_doc);

            let w = Window::new()
                .title("QML")
                .position((100.0, 100.0))
                .resizeable(true)
                .size(600.0, 600.0)
                .child(
                    MainViewState {
                        imports: qml_doc.imports,
                        qml: top_level.clone(),
                        rx: None,
                    }
                    .build(ctx),
                )
                .build(ctx);
            w
        })
        .run();
}
