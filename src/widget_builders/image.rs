use crate::parser::Value::{QmlIdent, QmlString};
use crate::parser::{Value, QML};
use crate::state;
use crate::widget_builders::{QmlContext, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;


pub struct ImageBuilder {}

impl WidgetBuilder for ImageBuilder {
    fn build(&self, properties: HashMap<String, Value>, _children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |id, ctx, row, col, qmlctx| -> Entity {
            let mut image = ImageWidget::new();
            image = image.attach(Grid::row(row as usize));
            image = image.attach(Grid::column(col as usize));

            let widget_id = match properties.get("id") {
                Some(QmlIdent(text)) => {
                    image = image.id(text.clone());
                    qmlctx.indexes.push(text.clone());
                    text.clone()
                }
                _ => String::new(),
            };

            let src = match properties.get("source") {
                Some(QmlString(src)) => src.clone(),
                _ => String::new(),
            };
            image = image.image(src);
            match properties.get("anchors.centerIn") {
                Some(QmlIdent(str)) => {
                    if str.eq("parent") {
                        image = image.v_align(Alignment::Center);
                        image = image.h_align(Alignment::Center);
                    }
                }
                _ => {}
            }
            return image.build(ctx);
        });
    }
}
