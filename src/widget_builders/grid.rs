use crate::parser::{Value, QML};
use crate::widget_builders::{parse_number, QmlContext, QmlWidgetBuilder, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;


pub struct GridBuilder {}

impl WidgetBuilder for GridBuilder {
    fn build(&self, properties: HashMap<String, Value>, children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |id, ctx, row, col, qmlctx| -> Entity {
            let mut grid = Grid::new();
            grid = grid.attach(Grid::row(row as usize));
            grid = grid.attach(Grid::column(col as usize));
            let rows = parse_number(properties.get("rows")) as u32;
            let cols = parse_number(properties.get("columns")) as u32;

            let mut grid_rows = Rows::create();
            for _i in 0..rows {
                grid_rows = grid_rows.push("stretch");
            }
            grid = grid.rows(grid_rows.build());

            let mut grid_cols = Columns::create();
            for _i in 0..rows {
                grid_cols = grid_cols.push("stretch");
            }
            grid = grid.columns(grid_cols.build());

            let mut grow = 0u32;
            let mut gcol = 0u32;

            let qwb = QmlWidgetBuilder::new();
            for (child_type, child_qml) in children.iter() {
                grid = grid.child(qwb.build(id, grow as usize, gcol as usize, child_type.clone(), child_qml.clone(), ctx, qmlctx));
                grow += 1;
                if grow as u32 == rows {
                    grow = 0;
                    gcol = (gcol + 1) % cols;
                }
            }
            return grid.build(ctx);
        });
    }
}
