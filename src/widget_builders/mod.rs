use crate::parser::Value::{QmlNumber, QmlString};
use crate::parser::{Value, QML};
use crate::widget_builders::button::ButtonBuilder;
use crate::widget_builders::grid::GridBuilder;
use crate::widget_builders::rectangle::RectangleBuilder;
use crate::widget_builders::textedit::TextEditBuilder;
use crate::widget_builders::textfield::TextBuilder;
use orbtk::prelude::*;


use crate::widget_builders::combobox::ComboBoxBuilder;
use crate::widget_builders::image::ImageBuilder;
use crate::widget_builders::import::ImportBuilder;
use std::collections::HashMap;

pub mod button;
pub mod combobox;
pub mod grid;
pub mod image;
pub mod import;
pub mod rectangle;
pub mod textedit;
pub mod textfield;

pub fn parse_color_property(properties: &HashMap<String, Value>, prop: &str) -> Color {
    match properties.get(prop) {
        Some(QmlString(col)) => match col.as_str() {
            "red" => Color::rgb(0xff, 00, 00),
            "blue" => Color::rgb(0x00, 00, 0xff),
            _ => Color::from(col.as_str()),
        },
        _ => Color::rgb(0xff, 0xff, 0xff),
    }
}

fn parse_number(val: Option<&Value>) -> f64 {
    match val {
        Some(QmlNumber(num)) => num.clone(),
        _ => 0.0,
    }
}

#[derive(Clone)]
pub struct QmlContext {
    pub(crate) indexes: Vec<String>,
    pub(crate) code: HashMap<(String, String), String>,
}

pub trait WidgetBuilder {
    fn build(&self, properties: HashMap<String, Value>, children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity>;
}

pub struct QmlWidgetBuilder {
    widgets: HashMap<String, Box<dyn WidgetBuilder>>,
}

impl QmlWidgetBuilder {
    pub fn new() -> QmlWidgetBuilder {
        let mut qwb = QmlWidgetBuilder { widgets: Default::default() };
        qwb.widgets.insert(String::from("Rectangle"), Box::new(RectangleBuilder {}));
        qwb.widgets.insert(String::from("Grid"), Box::new(GridBuilder {}));
        qwb.widgets.insert(String::from("Text"), Box::new(TextBuilder {}));
        qwb.widgets.insert(String::from("TextField"), Box::new(TextEditBuilder {}));
        qwb.widgets.insert(String::from("Button"), Box::new(ButtonBuilder {}));
        qwb.widgets.insert(String::from("Image"), Box::new(ImageBuilder {}));
        qwb.widgets.insert(String::from("ComboBox"), Box::new(ComboBoxBuilder {}));
        qwb
    }

    pub fn build(&self, id: Entity, row: usize, col: usize, widget_type: String, qml: QML, ctx: &mut BuildContext, qmlctx: &mut QmlContext) -> Entity {
        match self.widgets.get(&widget_type) {
            Some(builder) => {
                let wfn = builder.build(qml.properties, qml.children);
                let widget = wfn(id, ctx, row, col, qmlctx);

                return widget;
            }
            _ => {
                let builder = ImportBuilder { source: widget_type };
                let wfn = builder.build(qml.properties, qml.children);
                let widget = wfn(id, ctx, row, col, qmlctx);
                return widget;
            }
        }
        return ctx.create_entity();
    }
}
