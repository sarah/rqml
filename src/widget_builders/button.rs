use crate::parser::Value::{QmlIdent, QmlString};
use crate::parser::{Value, QML};
use crate::state;
use crate::widget_builders::{QmlContext, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;


pub struct ButtonBuilder {}

impl WidgetBuilder for ButtonBuilder {
    fn build(&self, properties: HashMap<String, Value>, _children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |id, ctx, row, col, _ids| -> Entity {
            let mut button = Button::new();
            button = button.attach(Grid::row(row as usize));
            button = button.attach(Grid::column(col as usize));

            let code = match properties.get("onclick").unwrap() {
                QmlString(code) => code.clone(),
                _ => String::new(),
            };

            button = button.on_click(move |states, _| -> bool {
                state(id, states).action(code.clone());
                return true;
            });

            let text = match properties.get("text").unwrap() {
                QmlString(text) => text.clone(),
                _ => String::new(),
            };

            match properties.get("anchors.centerIn") {
                Some(QmlIdent(str)) => {
                    if str.eq("parent") {
                        button = button.v_align(Alignment::Center);
                        button = button.h_align(Alignment::Center);
                    }
                }
                _ => {}
            }
            button = button.text(text);
            return button.build(ctx);
        });
    }
}
