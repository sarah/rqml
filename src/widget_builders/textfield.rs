use crate::parser::Value::{QmlIdent, QmlString};
use crate::parser::{Value, QML};
use crate::widget_builders::{QmlContext, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;

pub struct TextBuilder {}

impl WidgetBuilder for TextBuilder {
    fn build(&self, properties: HashMap<String, Value>, _children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |_id, ctx, row, col, qmlctx| -> Entity {
            let text = match properties.get("text") {
                Some(QmlString(text)) => text.clone(),
                _ => String::new(),
            };

            let mut tt = TextBlock::new();
            tt = tt.text(text);
            tt = tt.attach(Grid::row(row as usize));
            tt = tt.attach(Grid::column(col as usize));

            match properties.get("id") {
                Some(QmlIdent(text)) => {
                    tt = tt.id(text.clone());
                    qmlctx.indexes.push(text.clone())
                }
                _ => {}
            };

            match properties.get("anchors.centerIn") {
                Some(QmlIdent(str)) => {
                    if str.eq("parent") {
                        tt = tt.v_align(Alignment::Center);
                        tt = tt.h_align(Alignment::Center);
                    }
                }
                _ => {}
            }
            let entity = tt.build(ctx);
            return entity;
        });
    }
}
