use crate::parser::Value::{QmlIdent, QmlString};
use crate::parser::{Value, QML};
use crate::state;
use crate::widget_builders::{parse_number, QmlContext, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;
use rhai::{Array, Engine};
use std::ops::Index;

pub struct ComboBoxBuilder {}

impl WidgetBuilder for ComboBoxBuilder {
    fn build(&self, properties: HashMap<String, Value>, _children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |id, ctx, row, col, qmlctx| -> Entity {
            let mut combo_box = ComboBox::new();
            combo_box = combo_box.attach(Grid::row(row as usize));
            combo_box = combo_box.attach(Grid::column(col as usize));

            let widget_id = match properties.get("id") {
                Some(QmlIdent(text)) => {
                    combo_box = combo_box.id(text.clone());
                    qmlctx.indexes.push(text.clone());
                    text.clone()
                }
                _ => String::new(),
            };

            let items = match properties.get("items").unwrap() {
                QmlString(code) => {
                    qmlctx.code.insert((widget_id, String::from("items")), code.to_string());
                    let engine = Engine::new();
                    match engine.eval::<Array>(code.as_str()) {
                        Ok(list) => {
                            println!("Found Combobox with {} items {:?}", list.len(), list);
                            list.to_vec()
                        }
                        _ => vec![],
                    }
                }
                _ => vec![],
            };

            combo_box = combo_box.count(items.len());

            combo_box = combo_box.items_builder(move |bc, index| {
                let text = items.index(index);
                TextBlock::new().margin((0., 0., 0., 0.)).v_align("center").text(text.to_string()).build(bc)
            });

            let code = match properties.get("onselect").unwrap() {
                QmlString(code) => code.clone(),
                _ => String::new(),
            };

            combo_box = combo_box.on_changed(move |states, _, _ | {
                state(id, states).action(code.clone());
            });

            let selected = parse_number(properties.get("selected")) as u32;
            combo_box = combo_box.selected_index(selected as i32);

            match properties.get("anchors.centerIn") {
                Some(QmlIdent(str)) => {
                    if str.eq("parent") {
                        combo_box = combo_box.v_align(Alignment::Center);
                        combo_box = combo_box.h_align(Alignment::Center);
                    }
                }
                _ => {}
            }
            return combo_box.build(ctx);
        });
    }
}
