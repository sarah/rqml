use crate::parser::Value::QmlString;
use crate::parser::{parse_qml, Value, QML};
use crate::widget_builders::{parse_number, QmlContext, QmlWidgetBuilder, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;
use std::borrow::BorrowMut;

pub struct ImportBuilder {
    pub(crate) source: String,
}

impl WidgetBuilder for ImportBuilder {
    fn build(&self, properties: HashMap<String, Value>, children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        let import_source = self.source.clone();
        return Box::new(move |id, ctx, row, col, qmlctx| -> Entity {
            let qml_doc = parse_qml(format!("./res/{}.qml", import_source).as_str());
            let top_level = qml_doc.children.clone();
            QmlWidgetBuilder::new().build(id, row, col, top_level[0].0.clone(), top_level[0].1.clone(), ctx, qmlctx.borrow_mut())
        });
    }
}
