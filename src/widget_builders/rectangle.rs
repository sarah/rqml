use crate::parser::Value::QmlString;
use crate::parser::{Value, QML};
use crate::widget_builders::{parse_color_property, parse_number, QmlContext, QmlWidgetBuilder, WidgetBuilder};
use orbtk::prelude::HashMap;
use orbtk::prelude::*;

pub struct RectangleBuilder {}

impl WidgetBuilder for RectangleBuilder {
    fn build(&self, properties: HashMap<String, Value>, children: Vec<(String, QML)>) -> Box<dyn Fn(Entity, &mut BuildContext, usize, usize, &mut QmlContext) -> Entity> {
        return Box::new(move |id, ctx, row, col, qmlctx| -> Entity {
            let _width = parse_number(properties.get("width"));
            let _height = parse_number(properties.get("height"));

            let mut rect = Container::new();
            rect = rect.background(parse_color_property(&properties, "color"));
            rect = rect.border_brush(parse_color_property(&properties, "border.color"));
            rect = rect.border_width(Thickness::new(1.0, 1.0, 1.0, 1.0));
            rect = rect.attach(Grid::row(row as usize));
            rect = rect.attach(Grid::column(col as usize));
            let qwb = QmlWidgetBuilder::new();
            for (child_type, child_qml) in children.iter() {
                rect = rect.child(qwb.build(id, 0, 0, child_type.clone(), child_qml.clone(), ctx, qmlctx));
            }

            return rect.build(ctx);
        });
    }
}
