Grid {
    rows: 2
    columns: 2

    HelloTwitter {

    }


    HelloTwitter {

    }

    Rectangle {
        color: "#634B72"
        Text {
            id: target
            anchors.centerIn: parent
            text: "Hello, World!"
        }
    }

    Grid {
        rows: 2
        columns: 2

        Rectangle {
            color: "#634B72"
            TextField {
                id: tf1
                anchors.centerIn: parent
                text: "Test"
                onchange = [[
                ]]
            }
        }
        Rectangle {
            color: "#6F4985"
            Button {
                anchors.centerIn: parent
                text: "Hello, World!"
                onclick = [[
                       print("Hello World!");
                       update_property("target", "text", get_property("tf1","text"));
                ]]
            }
        }
        Rectangle {
            color: "#b29dbe"
            ComboBox {
                id: combobox
                anchors.centerIn: parent
                items =  [[
                     ["1", "World", "@@#$@#"]
                ]]
                selected: 2
                onselect = [[
                   update_property("target", "text", get_property("combobox","selected"));
                ]]
            }
        }
        Rectangle {
            color: "#F3F0F4"
            Button {
                anchors.centerIn: parent
                text: "Hello, World!"
                onclick = [[
                       print("Hello World!");
                       update_property("target", "text", "123");
                ]]
            }
        }

    }
}