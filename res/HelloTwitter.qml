Grid {
    rows:2
    columns:2
    Rectangle {
        color: "#F3F0F4"
        Text {
            anchors.centerIn: parent
            text: "Hello, Twitter! 🦀"
        }
    }
    Rectangle {
        color: "#281038"
        border.color: "#550099"
        anchors.centerIn: parent
        Image {
            anchors.centerIn: parent
            source: "./res/openprivacy.png"
        }
    }
    Rectangle {
        color: "#634B72"
    }
    Rectangle {
        color: "#281038"
        Text {
            anchors.centerIn: parent
            text: "Hello, Twitter! 🦀"
        }
    }
}
